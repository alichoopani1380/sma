FROM brainnco/elixir-rust-alpine:latest as builder

WORKDIR /app

# Install Hex + Rebar
RUN mix do local.hex --force, local.rebar --force

# Set args

ENV MIX_ENV=prod
ENV SECRET_KEY_BASE_MONITORING=nothing
ENV SECRET_KEY_BASE_ACCOUNT=nothing
ENV DB_NAME=nothing
ENV DB_USERNAME=nothing
ENV DB_PASSWORD=nothing
ENV DB_ENDPOINT=http://nothing:80

# First Copy
COPY mix.exs /app/
COPY mix.* /app/
COPY apps/account/mix.exs /app/apps/account/
COPY apps/account_web/mix.exs /app/apps/account_web/
COPY apps/monitoring/mix.exs /app/apps/monitoring/
COPY apps/monitoring_web/mix.exs /app/apps/monitoring_web/


RUN mix do deps.get --only prod


COPY config/ /app/config/
RUN MIX_ENV=prod mix do deps.compile

FROM brainnco/elixir-rust-alpine:latest as rel

RUN MIX_ENV=prod mix do local.hex --force, local.rebar --force

WORKDIR "/app"
RUN chown nobody /app

# USER nobody

ENV MIX_ENV=prod
ARG SECRET_KEY_BASE_MONITORING
ARG SECRET_KEY_BASE_ACCOUNT
ARG DB_NAME
ARG DB_USERNAME
ARG DB_PASSWORD
ARG DB_ENDPOINT


COPY --from=builder --chown=nobody:root /app/ /app/

# Final copy
COPY . /app/

RUN MIX_ENV=prod mix compile && mix release

# Start server
WORKDIR /app/_build/prod/rel/sma_umbrella/bin
CMD ["./sma_umbrella", "start"]

FROM brainnco/elixir-rust-alpine:latest

WORKDIR "/app"
RUN chown nobody /app
COPY --from=rel --chown=nobody:root /app/_build/prod/rel/sma_umbrella /app/

CMD ["./bin/sma_umbrella", "start"]
