defmodule Monitoring.LogsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Monitoring.Logs` context.
  """

  @doc """
  Generate a endpoint_log.
  """
  def endpoint_log_fixture(attrs \\ %{}) do
    {:ok, endpoint_log} =
      attrs
      |> Enum.into(%{

      })
      |> Monitoring.Logs.create_endpoint_log()

    endpoint_log
  end

  @doc """
  Generate a warning.
  """
  def warning_fixture(attrs \\ %{}) do
    {:ok, warning} =
      attrs
      |> Enum.into(%{
        monitor_key: "some monitor_key"
      })
      |> Monitoring.Logs.create_warning()

    warning
  end
end
