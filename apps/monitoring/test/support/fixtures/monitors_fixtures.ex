defmodule Monitoring.MonitorsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Monitoring.Monitors` context.
  """

  @doc """
  Generate a monitor.
  """
  def monitor_fixture(attrs \\ %{}) do
    {:ok, monitor} =
      attrs
      |> Enum.into(%{

      })
      |> Monitoring.Monitors.create_monitor()

    monitor
  end
end
