defmodule Monitoring.LogsTest do
  use Monitoring.DataCase

  alias Monitoring.Logs

  describe "endpoint_logs" do
    alias Monitoring.Logs.EndpointLog

    import Monitoring.LogsFixtures

    @invalid_attrs %{}

    test "list_endpoint_logs/0 returns all endpoint_logs" do
      endpoint_log = endpoint_log_fixture()
      assert Logs.list_endpoint_logs() == [endpoint_log]
    end

    test "get_endpoint_log!/1 returns the endpoint_log with given id" do
      endpoint_log = endpoint_log_fixture()
      assert Logs.get_endpoint_log!(endpoint_log.id) == endpoint_log
    end

    test "create_endpoint_log/1 with valid data creates a endpoint_log" do
      valid_attrs = %{}

      assert {:ok, %EndpointLog{} = endpoint_log} = Logs.create_endpoint_log(valid_attrs)
    end

    test "create_endpoint_log/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Logs.create_endpoint_log(@invalid_attrs)
    end

    test "update_endpoint_log/2 with valid data updates the endpoint_log" do
      endpoint_log = endpoint_log_fixture()
      update_attrs = %{}

      assert {:ok, %EndpointLog{} = endpoint_log} = Logs.update_endpoint_log(endpoint_log, update_attrs)
    end

    test "update_endpoint_log/2 with invalid data returns error changeset" do
      endpoint_log = endpoint_log_fixture()
      assert {:error, %Ecto.Changeset{}} = Logs.update_endpoint_log(endpoint_log, @invalid_attrs)
      assert endpoint_log == Logs.get_endpoint_log!(endpoint_log.id)
    end

    test "delete_endpoint_log/1 deletes the endpoint_log" do
      endpoint_log = endpoint_log_fixture()
      assert {:ok, %EndpointLog{}} = Logs.delete_endpoint_log(endpoint_log)
      assert_raise Ecto.NoResultsError, fn -> Logs.get_endpoint_log!(endpoint_log.id) end
    end

    test "change_endpoint_log/1 returns a endpoint_log changeset" do
      endpoint_log = endpoint_log_fixture()
      assert %Ecto.Changeset{} = Logs.change_endpoint_log(endpoint_log)
    end
  end

  describe "warnings" do
    alias Monitoring.Logs.Warning

    import Monitoring.LogsFixtures

    @invalid_attrs %{monitor_key: nil}

    test "list_warnings/0 returns all warnings" do
      warning = warning_fixture()
      assert Logs.list_warnings() == [warning]
    end

    test "get_warning!/1 returns the warning with given id" do
      warning = warning_fixture()
      assert Logs.get_warning!(warning.id) == warning
    end

    test "create_warning/1 with valid data creates a warning" do
      valid_attrs = %{monitor_key: "some monitor_key"}

      assert {:ok, %Warning{} = warning} = Logs.create_warning(valid_attrs)
      assert warning.monitor_key == "some monitor_key"
    end

    test "create_warning/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Logs.create_warning(@invalid_attrs)
    end

    test "update_warning/2 with valid data updates the warning" do
      warning = warning_fixture()
      update_attrs = %{monitor_key: "some updated monitor_key"}

      assert {:ok, %Warning{} = warning} = Logs.update_warning(warning, update_attrs)
      assert warning.monitor_key == "some updated monitor_key"
    end

    test "update_warning/2 with invalid data returns error changeset" do
      warning = warning_fixture()
      assert {:error, %Ecto.Changeset{}} = Logs.update_warning(warning, @invalid_attrs)
      assert warning == Logs.get_warning!(warning.id)
    end

    test "delete_warning/1 deletes the warning" do
      warning = warning_fixture()
      assert {:ok, %Warning{}} = Logs.delete_warning(warning)
      assert_raise Ecto.NoResultsError, fn -> Logs.get_warning!(warning.id) end
    end

    test "change_warning/1 returns a warning changeset" do
      warning = warning_fixture()
      assert %Ecto.Changeset{} = Logs.change_warning(warning)
    end
  end
end
