defmodule Monitoring.Logs.EndpointLog do
  use ArangoXEcto.Schema
  import Ecto.Changeset

  schema "endpoint_logs" do
    field :monitor_key, :string
    field :response_status_code, :integer
    field :requested_at, :string

    timestamps()
  end

  @doc false
  def changeset(endpoint_log, attrs) do
    endpoint_log
    |> cast(attrs, [:monitor_key, :response_status_code, :requested_at])
  end
end
