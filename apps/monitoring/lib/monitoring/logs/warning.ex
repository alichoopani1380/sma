defmodule Monitoring.Logs.Warning do
  use ArangoXEcto.Schema
  import Ecto.Changeset

  schema "warnings" do
    field :monitor_key, :string
    field :start_time, :string
    field :end_time, :string
    field :failures_count, :integer
  end

  @doc false
  def changeset(warning, attrs) do
    warning
    |> cast(attrs, [:monitor_key, :start_time, :end_time, :failures_count])
  end
end
