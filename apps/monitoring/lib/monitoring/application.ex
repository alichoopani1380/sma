defmodule Monitoring.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Monitoring.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: Monitoring.PubSub},
      # Start a worker by calling: Monitoring.Worker.start_link(arg)
      # {Monitoring.Worker, arg},
      {Registry, [keys: :unique, name: :monitor_worker_registry]},
      {Monitoring.Workers.MonitorManager, strategy: :one_for_one, name: Monitoring.Workers.MonitorManager},
      Monitoring.Workers.DBSyncer
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Monitoring.Supervisor)
  end
end
