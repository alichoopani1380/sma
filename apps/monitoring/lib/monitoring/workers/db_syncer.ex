defmodule Monitoring.Workers.DBSyncer do
  use GenServer
  require Logger
  alias Monitoring.Workers.MonitorManager

  @period Application.fetch_env!(:monitoring, :db_syncer_period) * 1000

  @impl true
  def init(state) do
    send(self(), :scheduler_job)
    {:ok, state}
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil)
  end

  @impl true
  def handle_cast(:sync_db, state) do
    Logger.info "DBSyncer start to sync..."

    {:ok, monitors_id_list} = Monitoring.Monitors.get_active_monitors_id_list()

    all_live_workers = Registry.select(:monitor_worker_registry, [{{:"$1", :_, :"$2"}, [{:"==", :"$2", "worker"}], [:"$1"]}])

    #add non-existing workers
    for monitor_id <- (monitors_id_list -- all_live_workers) do
        {:ok, monitor} = Monitoring.Monitors.get_monitor_credential_by_id(monitor_id)
        MonitorManager.start_child(monitor)
    end

    #delete canceled live workers
    for monitor_id <- all_live_workers -- monitors_id_list do
      MonitorManager.delete_worker(monitor_id)
    end

    Logger.info "DBSyncer finished syncing."
    {:noreply, state}
  end

  @impl true
  def handle_info(:scheduler_job, state) do
    GenServer.cast(self(), :sync_db)
    schedule_work()
    {:noreply, state}
  end

  defp schedule_work() do
    Process.send_after(self(), :scheduler_job, @period)
  end
end
