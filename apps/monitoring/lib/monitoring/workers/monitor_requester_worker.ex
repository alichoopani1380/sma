defmodule Monitoring.Workers.MonitorRequesterWorker do
  use GenServer
  require Logger

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: via_tuple(state))
  end

  def child_spec(state) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [state]},
      restart: :transient
    }
  end

  @impl true
  def init(state) do
    send(self(), :scheduler_job)
    {:ok, state}
  end

  @impl true
  def handle_cast(:send_req, %{endpoint: endpoint, request_method: request_method} = state) do
    Logger.info "send request to endpoint \"#{state.id}\""

    #TODO do something if logger not found
    [{logger_pid, _}] = Registry.lookup(:monitor_worker_registry, "#{state.id}_logger")

    res = send_http_request(endpoint, request_method)

    GenServer.cast(logger_pid, {:log, res})


    {:noreply, state}
  end

  @impl true
  def handle_info(:scheduler_job, %{period: period} = state) do
    GenServer.cast(self(), :send_req)
    schedule_work(period)
    {:noreply, state}
  end

  defp schedule_work(period) do
    Process.send_after(self(), :scheduler_job, period * 1000)
  end

  defp via_tuple(state) do
    {:via, Registry, {:monitor_worker_registry, "#{state.id}_requester", "requester"}}
  end

  defp send_http_request(endpoint, method) do

    time = get_time()

    f = case method do
      "GET" -> &HTTPoison.get/2
      "POST" -> &HTTPoison.post/2
      _ -> &HTTPoison.get/2
    end

    case f.(endpoint, []) do
      {:ok, response} ->
        %{response_status_code: response.status_code, requested_at: time}
      _ ->
        %{response_status_code: 0, requested_at: time}
    end
  end

  def get_time do
    time = DateTime.utc_now()
    "#{time.year}-#{time.month}-#{time.day}T#{time.hour}:#{time.minute}:#{time.second}"
  end
end
