defmodule Monitoring.Workers.MonitorLoggerWorker do
  use GenServer
  require Logger

  @logs_batch_insert_count Application.get_env(:monitoring, :logs_batch_insert_count) - 1

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: via_tuple(state))
  end

  def child_spec(state) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [state]},
      restart: :transient
    }
  end

  @impl true
  def init(state) do
    {:ok, state}
  end

  # add new log
  @impl true
  def handle_cast({:log, log}, %{monitor: monitor, logs: logs, failures: failures, logs_count: @logs_batch_insert_count}) do

    logs = [log | logs]

    Logger.info "logs batch insert for monitor \"#{monitor.id}\""

    failures = handle_failures(log, failures, monitor)

    Monitoring.Logs.batch_insert_logs(logs |> Enum.reverse, monitor)

    {:noreply, %{monitor: monitor, logs: [], failures: failures, logs_count: 0}}
  end

  # add new log
  @impl true
  def handle_cast({:log, log}, %{monitor: monitor, logs: logs, failures: failures, logs_count: logs_count}) do
    failures = handle_failures(log, failures, monitor)
    {:noreply, %{monitor: monitor, logs: [log | logs], failures: failures, logs_count: logs_count + 1}}
  end

  defp handle_failures(
    %{response_status_code: response_status_code, requested_at: requested_at},
    %{failures_count: failures_count, start_time: start_time},
    %{id: monitor_id, warning_overflow: warning_overflow}) do
    is_fail_log = cond do
      #TODO handle spected_status_code
      response_status_code < 200 or response_status_code > 299 -> true
      true -> false
    end

    if is_fail_log do
      failures_count = failures_count + 1

      cond do
        failures_count == 1 ->
          %{failures_count: failures_count, start_time: requested_at}
        failures_count == warning_overflow ->
          Monitoring.Logs.create_warning(%{monitor_key: monitor_id, start_time: start_time, end_time: requested_at, failures_count: warning_overflow})
          Logger.info "warning for monitor \"#{monitor_id}\""
          %{failures_count: 0, start_time: nil}
        true ->
          %{failures_count: failures_count, start_time: start_time}
      end

    else
      %{failures_count: 0, start_time: nil}
    end
  end

  defp via_tuple(state) do
    {:via, Registry, {:monitor_worker_registry, "#{state.monitor.id}_logger", "logger"}}
  end

  def get_time do
    time = DateTime.utc_now()
    "#{time.year}-#{time.month}-#{time.day}T#{time.hour}:#{time.minute}:#{time.second}"
  end
end
