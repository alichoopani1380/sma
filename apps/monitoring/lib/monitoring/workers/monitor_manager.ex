defmodule Monitoring.Workers.MonitorManager do
  # Automatically defines child_spec/1
  use DynamicSupervisor

  def start_link(init_arg) do
    DynamicSupervisor.start_link(__MODULE__, init_arg, name: __MODULE__)
  end

  @impl true
  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def count_children() do
    DynamicSupervisor.count_children(__MODULE__)
  end

  def start_child(monitor_worker_init_state) do
    #create logger worker
    monitor_logger_worker_child_specification = {Monitoring.Workers.MonitorLoggerWorker, %{monitor: monitor_worker_init_state, logs: [], failures: %{failures_count: 0, start_time: nil}, logs_count: 0}}
    DynamicSupervisor.start_child(__MODULE__, monitor_logger_worker_child_specification)

    #create requester
    monitor_requester_worker_child_specification = {Monitoring.Workers.MonitorRequesterWorker, monitor_worker_init_state}
    DynamicSupervisor.start_child(__MODULE__, monitor_requester_worker_child_specification)
  end

  def delete_worker(key) do
    DynamicSupervisor.terminate_child(__MODULE__, Registry.lookup(:monitor_worker_registry, "#{key}_requester"))
    #TODO inset logs of logger then terminate
    DynamicSupervisor.terminate_child(__MODULE__, Registry.lookup(:monitor_worker_registry, "#{key}_logger"))
  end

end
