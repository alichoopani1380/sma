defmodule Monitoring.Monitors.Monitor do
  use ArangoXEcto.Schema
  import Ecto.Changeset

  schema "monitors" do
    field :user_id, :string

    field :endpoint, :string
    field :request_custom_headers
    field :request_method, :string, default: "GET"
    field :request_body, :string
    field :period, :integer, default: 10
    field :spected_status_code, :integer, default: 200
    field :warning_webhook, :string
    field :warning_overflow, :integer, default: 10

    timestamps()
  end

  @doc false
  def changeset(monitor, attrs) do
    monitor
    |> cast(attrs, [])
    |> validate_required([])
  end

  def create_changeset(monitor, attrs) do
    monitor
    |> cast(attrs, [:user_id, :endpoint, :request_custom_headers, :request_method, :request_body, :period, :spected_status_code, :warning_webhook, :warning_overflow])
    |> validate_required([:endpoint, :period, :spected_status_code])
  end
end
