defmodule Monitoring.Monitors do
  @moduledoc """
  The Monitors context.
  """

  import Ecto.Query, warn: false
  alias Monitoring.Repo

  alias Monitoring.Monitors.Monitor
  alias Monitoring.Workers.MonitorManager

  @doc """
  Returns the list of monitors.

  ## Examples

      iex> list_monitors()
      [%Monitor{}, ...]

  """
  def list_monitors_of_user(user_id) do
    case ArangoXEcto.aql_query(
        Repo,
        """
        FOR m in monitors FILTER m.user_id == "#{user_id}" RETURN m
        """
      ) do
        {:ok, res} ->
          {
            :ok,
            Enum.map(res, &ArangoXEcto.raw_to_struct(&1, Monitor))
          }
        _ -> {:error, :not_found}
      end
  end

  @doc """
  Gets a single monitor.

  Raises `Ecto.NoResultsError` if the Monitor does not exist.

  ## Examples

      iex> get_monitor!(123)
      %Monitor{}

      iex> get_monitor!(456)
      ** (Ecto.NoResultsError)

  """
  def get_monitor!(id), do: Repo.get!(Monitor, id)

  @doc """
  Creates a monitor.

  ## Examples

      iex> create_monitor(%{field: value})
      {:ok, %Monitor{}}

      iex> create_monitor(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_monitor(attrs \\ %{}) do
    user_id = Map.get(attrs, "user_id")
    case Account.Accounts.check_limit(user_id) do
      :ok ->
        user = Account.Accounts.get_user!(user_id)
        Account.Accounts.update_user(user, %{"monitors_count" => user.monitors_count + 1})
        
        {:ok, monitor} = %Monitor{}
        |> Monitor.create_changeset(attrs)
        |> Repo.insert()

        MonitorManager.start_child(Map.take(monitor, [:id, :period, :endpoint, :request_method, :warning_overflow]))

        {:ok, monitor}
      _ ->
        {:error, :endpoint_limit}
    end
  end

  defp increment_monitors_count(user_id) do
    aql = """

    """
    ArangoXEcto.aql_query(
        Repo,
        """
        FOR u IN users
        FILTER u._key == "#{user_id}"
        LIMIT 1
        UPDATE u._key WITH {monitors_count: SUM([u.monitors_count, 1])} IN users OPTIONS { exclusive: true }
        """
      )
  end

  @spec get_monitor_credential_by_id(any) :: {:error, :not_found}
  def get_monitor_credential_by_id(monitor_id) do
    case ArangoXEcto.aql_query(
        Repo,
        """
        FOR m in monitors FILTER m._key == "#{monitor_id}" LIMIT 1 RETURN KEEP(m, "_key", "endpoint", "request_method", "period", "warning_overflow")
        """
      ) do
        {:ok, [res]} ->
          monitor = for {key, val} <- res, into: %{}, do: {String.to_atom(key), val}
          {:ok, Map.put(monitor, :id, monitor._key)}
        _ -> {:error, :not_found}
      end
  end

  def get_active_monitors_id_list() do
    x = ArangoXEcto.aql_query(
        Repo,
        """
        FOR m in monitors RETURN m._key
        """
      )
  end


  @doc """
  Updates a monitor.

  ## Examples

      iex> update_monitor(monitor, %{field: new_value})
      {:ok, %Monitor{}}

      iex> update_monitor(monitor, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_monitor(%Monitor{} = monitor, attrs) do
    monitor
    |> Monitor.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a monitor.

  ## Examples

      iex> delete_monitor(monitor)
      {:ok, %Monitor{}}

      iex> delete_monitor(monitor)
      {:error, %Ecto.Changeset{}}

  """
  def delete_monitor(%Monitor{} = monitor) do
    Repo.delete(monitor)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking monitor changes.

  ## Examples

      iex> change_monitor(monitor)
      %Ecto.Changeset{data: %Monitor{}}

  """
  def change_monitor(%Monitor{} = monitor, attrs \\ %{}) do
    Monitor.changeset(monitor, attrs)
  end
end
