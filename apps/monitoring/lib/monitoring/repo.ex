defmodule Monitoring.Repo do
  use Ecto.Repo,
    otp_app: :monitoring,
    adapter: ArangoXEcto.Adapter
end
