defmodule Monitoring.Logs do
  @moduledoc """
  The Logs context.
  """

  import Ecto.Query, warn: false
  alias Monitoring.Repo

  alias Monitoring.Logs.EndpointLog
  alias Monitoring.Logs.Warning

  @doc """
  Returns the list of endpoint_logs.

  ## Examples

      iex> list_endpoint_logs()
      [%EndpointLog{}, ...]

  """
  def list_endpoint_logs(user_id, monitor_id) do
    aql = """
    LET monitor_validity = (
    FOR monitor IN monitors
    FILTER monitor._key == "#{monitor_id}"
    FILTER monitor.user_id == "#{user_id}"
    LIMIT 1
    RETURN true
    )

    LET logs_list = (
    FOR log IN endpoint_logs
    FILTER log.monitor_key == "#{monitor_id}"
    RETURN log
    )

    RETURN monitor_validity != [] ? {logs: logs_list} : {error: "not_found"}
    """

    case ArangoXEcto.aql_query(
        Repo,
        aql
      ) do
        {:ok, [res]} ->

          case res do
            %{"logs" => logs_list} ->
              {:ok, res}
            _ ->
              {:error, :not_found}
          end
        _ -> {:error, :not_found}
      end

    Repo.all(EndpointLog)
  end

  def daily_reports(user_id, monitor_id, date) do
    aql = """
    LET access = (
        FOR m IN monitors
            FILTER m._key == "#{monitor_id}" AND m.user_id == "#{user_id}"
            RETURN KEEP(m, "_key")
    )

    LET reports = (
        FOR log IN endpoint_logs
              FILTER log.monitor_key == "#{monitor_id}"
              FILTER DATE_TRUNC("#{date}", 'day') == DATE_TRUNC(DATE_ISO8601(log.requested_at), 'day')
              RETURN KEEP(log, "response_status_code")
    )

    RETURN access == [] ? {error: "access_denied"} : {reports: reports}
    """

    case ArangoXEcto.aql_query(
        Repo,
        aql
      ) do
        {:ok, [%{"error" => "access_denied"}]} ->
          {:error, :access_denied}
        {:ok, [%{"reports" => reports}]} ->
          {:ok, logs_report(reports)}
        _ ->
          {:error, :not_found}
      end
  end

  defp logs_report(logs) do
    Enum.reduce(
      logs,
      %{},
      fn x, acc ->
        Map.update(acc, Map.get(x, "response_status_code", "unknown"), 1, &(&1 + 1))
      end
    )
    |> Enum.reduce(
      [],
      fn {key, value}, acc -> [%{"status_code" => key, "count" => value} | acc] end
    )
  end

  def list_warnings_by_monitor_id(monitor_id, user_id) do
    aql = """
    LET access = (
        FOR m IN monitors
            FILTER m._key == "#{monitor_id}" AND m.user_id == "#{user_id}"
            RETURN KEEP(m, "_key")
        )
    LET ws = (
        FOR w IN warnings
            FILTER w.monitor_key == "#{monitor_id}"
            RETURN w
        )

    RETURN access == [] ? {error: "access_denied"} : {warnings: ws}
    """
    case ArangoXEcto.aql_query(
      Repo,
      aql
    ) do
      {:ok, [%{"error" => "access_denied"}]} ->
        {:error, :access_denied}
      {:ok, [%{"warnings" => warnings}]} ->
        {:ok, Enum.map(warnings, &ArangoXEcto.raw_to_struct(&1, Warning))}
      _ ->
        {:error, :not_found}
    end

  end

  def list_warnings_by_date(user_id, date) do
    aql = """
    LET monitoring_keys = (
        FOR m IN monitors
            FILTER m.user_id == "#{user_id}"
            RETURN m._key
    )

    FOR w IN warnings
        FILTER w.monitor_key IN monitoring_keys
        FILTER DATE_TRUNC("#{date}", 'day') == DATE_TRUNC(w.start_time, 'day')
        COLLECT m_key = w.monitor_key INTO warningsByMonitorKey
        RETURN {monitor_key: m_key, warnings: warningsByMonitorKey[*].w}
    """
    case ArangoXEcto.aql_query(
      Repo,
      aql
    ) do
      {:ok, res} ->
        {:ok, res}
      _ ->
        {:error, :not_found}
    end

  end

  @doc """
  Gets a single endpoint_log.

  Raises `Ecto.NoResultsError` if the Endpoint log does not exist.

  ## Examples

      iex> get_endpoint_log!(123)
      %EndpointLog{}

      iex> get_endpoint_log!(456)
      ** (Ecto.NoResultsError)

  """
  def get_endpoint_log!(id), do: Repo.get!(EndpointLog, id)

  @doc """
  Creates a endpoint_log.

  ## Examples

      iex> create_endpoint_log(%{field: value})
      {:ok, %EndpointLog{}}

      iex> create_endpoint_log(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_endpoint_log(attrs \\ %{}) do
    %EndpointLog{}
    |> EndpointLog.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a endpoint_log.

  ## Examples

      iex> update_endpoint_log(endpoint_log, %{field: new_value})
      {:ok, %EndpointLog{}}

      iex> update_endpoint_log(endpoint_log, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_endpoint_log(%EndpointLog{} = endpoint_log, attrs) do
    endpoint_log
    |> EndpointLog.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a endpoint_log.

  ## Examples

      iex> delete_endpoint_log(endpoint_log)
      {:ok, %EndpointLog{}}

      iex> delete_endpoint_log(endpoint_log)
      {:error, %Ecto.Changeset{}}

  """
  def delete_endpoint_log(%EndpointLog{} = endpoint_log) do
    Repo.delete(endpoint_log)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking endpoint_log changes.

  ## Examples

      iex> change_endpoint_log(endpoint_log)
      %Ecto.Changeset{data: %EndpointLog{}}

  """
  def change_endpoint_log(%EndpointLog{} = endpoint_log, attrs \\ %{}) do
    EndpointLog.changeset(endpoint_log, attrs)
  end

  def batch_insert_logs(logs, monitor) do
    Repo.insert_all(EndpointLog, Enum.map(logs, fn log -> Map.merge(log, %{monitor_key: monitor.id}) end))
  end



  alias Monitoring.Logs.Warning

  @doc """
  Returns the list of warnings.

  ## Examples

      iex> list_warnings()
      [%Warning{}, ...]

  """
  def list_warnings do
    Repo.all(Warning)
  end

  @doc """
  Creates a warning.

  ## Examples

      iex> create_warning(%{field: value})
      {:ok, %Warning{}}

      iex> create_warning(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_warning(attrs \\ %{}) do
    %Warning{}
    |> Warning.changeset(attrs)
    |> Repo.insert()
  end

end
