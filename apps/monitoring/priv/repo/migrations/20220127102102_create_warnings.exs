defmodule Monitoring.Repo.Migrations.CreateWarnings do
  use Ecto.Migration

  def change do
    create table(:warnings) do
      add :monitor_key, :string

      timestamps()
    end
  end
end
