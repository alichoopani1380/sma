defmodule Monitoring.Repo.Migrations.CreateEndpointLogs do
  use Ecto.Migration

  def change do
    create table(:endpoint_logs) do

      timestamps()
    end
  end
end
