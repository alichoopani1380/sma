defmodule AccountWeb.UserController do
  use AccountWeb, :controller

  alias Account.Accounts
  alias Account.Accounts.User

  alias Account.Auth.Manager, as: AuthManager

  action_fallback AccountWeb.FallbackController

  def signup(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Accounts.init_user(user_params),
    {:ok, jwt, _claims} <- AuthManager.encode_and_sign(user) do
      conn
      |> put_status(:created)
      |> render("jwt.json", jwt: jwt)
    end
  end


  def signin(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Accounts.validate_username_password(user_params),
    {:ok, jwt, _claims} <- AuthManager.encode_and_sign(user) do
      conn
      |> put_status(:created)
      |> render("jwt.json", jwt: jwt)
    end
  end
end
