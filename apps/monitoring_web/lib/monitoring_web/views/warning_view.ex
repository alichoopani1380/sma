defmodule MonitoringWeb.WarningView do
  use MonitoringWeb, :view
  alias MonitoringWeb.WarningView

  def render("index.json", %{warnings: warnings}) do
    %{data: render_many(warnings, WarningView, "warning.json")}
  end

  def render("show.json", %{warning: warning}) do
    %{data: render_one(warning, WarningView, "warning.json")}
  end

  def render("warning.json", %{warning: warning}) do
    %{
      # id: warning.id,
      # monitor_key: warning.monitor_key,
      failures_count: warning.failures_count,
      start_time: warning.start_time,
      end_time: warning.end_time
    }
  end

  def render("warnings_list_group_by_monitor_key.json", %{data: data}) do
    %{data: Enum.map(data, fn item -> render("warnings_with_monitor_key.json", %{data: item}) end)}
  end

  def render("warnings_with_monitor_key.json", %{data: data}) do
    %{
      monitor_key: Map.get(data, "monitor_key"),
      warnings: Enum.map(
        Map.get(data, "warnings"),
        fn item ->
        render("warning.json", %{
          warning: (for ({key, val} <- item), into: (%{}), do: ({String.to_atom(key), val}))
        })
       end)
    }
  end
end
