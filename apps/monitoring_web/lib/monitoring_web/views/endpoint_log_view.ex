defmodule MonitoringWeb.EndpointLogView do
  use MonitoringWeb, :view
  alias MonitoringWeb.EndpointLogView

  def render("index.json", %{endpoint_logs: endpoint_logs}) do
    %{data: render_many(endpoint_logs, EndpointLogView, "endpoint_log.json")}
  end

  def render("show.json", %{endpoint_log: endpoint_log}) do
    %{data: render_one(endpoint_log, EndpointLogView, "endpoint_log.json")}
  end

  def render("daily_report.json", %{reports: reports}) do
    %{
      reports: reports
    }
  end

  def render("endpoint_log.json", %{endpoint_log: endpoint_log}) do
    %{
      id: endpoint_log.id,
      response_status_code: endpoint_log.response_status_code,
      time: endpoint_log.requested_at
    }
  end
end
