defmodule MonitoringWeb.MonitorView do
  use MonitoringWeb, :view
  alias MonitoringWeb.MonitorView

  def render("index.json", %{monitors: monitors}) do
    %{data: render_many(monitors, MonitorView, "monitor.json")}
  end

  def render("show.json", %{monitor: monitor}) do
    %{data: render_one(monitor, MonitorView, "monitor.json")}
  end

  def render("monitor.json", %{monitor: monitor}) do
    %{
      id: monitor.id,
      endpoint: monitor.endpoint,
      period: monitor.period,
      spected_status_code: monitor.spected_status_code
    }
  end
end
