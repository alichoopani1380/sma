defmodule MonitoringWeb.MonitorController do
  use MonitoringWeb, :controller

  alias Monitoring.Monitors
  alias Monitoring.Monitors.Monitor

  action_fallback MonitoringWeb.FallbackController

  def new_endpoint(%{assigns: %{user_id: user_id}} = conn, %{"monitor" => monitor_params}) do
    with {:ok, monitor} <- Monitors.create_monitor(Map.put(monitor_params, "user_id", user_id)) do
      conn
      |> put_status(:created)
      |> render("show.json", monitor: monitor)
    end
  end

  def monitors_list(%{assigns: %{user_id: user_id}} = conn, attrs) do
    with {:ok, monitors} <- Monitors.list_monitors_of_user(user_id) do
      render(conn, "index.json", monitors: monitors)
    end
  end

  def show(conn, %{"id" => id}) do
    monitor = Monitors.get_monitor!(id)
    render(conn, "show.json", monitor: monitor)
  end

  def update(conn, %{"id" => id, "monitor" => monitor_params}) do
    monitor = Monitors.get_monitor!(id)

    with {:ok, %Monitor{} = monitor} <- Monitors.update_monitor(monitor, monitor_params) do
      render(conn, "show.json", monitor: monitor)
    end
  end

  def delete(conn, %{"id" => id}) do
    monitor = Monitors.get_monitor!(id)

    with {:ok, %Monitor{}} <- Monitors.delete_monitor(monitor) do
      send_resp(conn, :no_content, "")
    end
  end
end
