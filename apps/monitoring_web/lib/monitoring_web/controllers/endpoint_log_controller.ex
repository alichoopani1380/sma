defmodule MonitoringWeb.EndpointLogController do
  use MonitoringWeb, :controller

  alias Monitoring.Logs
  alias Monitoring.Logs.EndpointLog

  action_fallback MonitoringWeb.FallbackController

  def daily_report(%{assigns: %{user_id: user_id}} = conn, %{"monitor_id" => monitor_id, "date" => date} = at) do
    with {:ok, reports} <- Logs.daily_reports(user_id, monitor_id, date) do
      render(conn, "daily_report.json", reports: reports)
    end
  end

  def list_monitor_logs(%{assigns: %{user_id: user_id}} = conn, %{"monitor_id" => monitor_id} = at) do
    IO.inspect(at)
    endpoint_logs = Logs.list_endpoint_logs(user_id, monitor_id)
    render(conn, "index.json", endpoint_logs: endpoint_logs)
  end

end
