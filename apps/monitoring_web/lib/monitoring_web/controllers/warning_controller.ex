defmodule MonitoringWeb.WarningController do
  use MonitoringWeb, :controller

  alias Monitoring.Logs
  alias Monitoring.Logs.Warning

  action_fallback MonitoringWeb.FallbackController

  def list_warnings(%{assigns: %{user_id: user_id}} = conn, %{"date" => date}) do
    with {:ok, data} <- Logs.list_warnings_by_date(user_id, date) do
      render(conn, "warnings_list_group_by_monitor_key.json", data: data)
    end
  end

  def list_warnings(%{assigns: %{user_id: user_id}} = conn, %{"monitor_id" => monitor_id}) do
    with {:ok, warnings} <- Logs.list_warnings_by_monitor_id(monitor_id, user_id) do
      render(conn, "index.json", warnings: warnings)
    end
  end

end
