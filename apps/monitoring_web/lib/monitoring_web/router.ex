defmodule MonitoringWeb.Router do
  use MonitoringWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :jwt_authenticated do
    plug Account.Auth.Pipeline
  end

  scope "/api", MonitoringWeb do
    pipe_through :api

    scope "/v1" do


      pipe_through :jwt_authenticated

      scope "/monitoring" do
        post "/", MonitorController, :new_endpoint
        get "/", MonitorController, :monitors_list
        get "/:monitor_id/logs/", EndpointLogController, :list_monitor_logs
        get "/:monitor_id/daily_report/:date", EndpointLogController, :daily_report
        get "/:monitor_id/warnings", WarningController, :list_warnings
      end

      get "/warning/:date", WarningController, :list_warnings
    end
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]

      live_dashboard "/dashboard", metrics: MonitoringWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through [:fetch_session, :protect_from_forgery]

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
