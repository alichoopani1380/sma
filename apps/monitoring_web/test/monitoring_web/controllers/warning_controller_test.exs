defmodule MonitoringWeb.WarningControllerTest do
  use MonitoringWeb.ConnCase

  import Monitoring.LogsFixtures

  alias Monitoring.Logs.Warning

  @create_attrs %{
    monitor_key: "some monitor_key"
  }
  @update_attrs %{
    monitor_key: "some updated monitor_key"
  }
  @invalid_attrs %{monitor_key: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all warnings", %{conn: conn} do
      conn = get(conn, Routes.warning_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create warning" do
    test "renders warning when data is valid", %{conn: conn} do
      conn = post(conn, Routes.warning_path(conn, :create), warning: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.warning_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "monitor_key" => "some monitor_key"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.warning_path(conn, :create), warning: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update warning" do
    setup [:create_warning]

    test "renders warning when data is valid", %{conn: conn, warning: %Warning{id: id} = warning} do
      conn = put(conn, Routes.warning_path(conn, :update, warning), warning: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.warning_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "monitor_key" => "some updated monitor_key"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, warning: warning} do
      conn = put(conn, Routes.warning_path(conn, :update, warning), warning: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete warning" do
    setup [:create_warning]

    test "deletes chosen warning", %{conn: conn, warning: warning} do
      conn = delete(conn, Routes.warning_path(conn, :delete, warning))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.warning_path(conn, :show, warning))
      end
    end
  end

  defp create_warning(_) do
    warning = warning_fixture()
    %{warning: warning}
  end
end
