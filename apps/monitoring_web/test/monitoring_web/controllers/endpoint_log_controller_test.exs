defmodule MonitoringWeb.EndpointLogControllerTest do
  use MonitoringWeb.ConnCase

  import Monitoring.LogsFixtures

  alias Monitoring.Logs.EndpointLog

  @create_attrs %{

  }
  @update_attrs %{

  }
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all endpoint_logs", %{conn: conn} do
      conn = get(conn, Routes.endpoint_log_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create endpoint_log" do
    test "renders endpoint_log when data is valid", %{conn: conn} do
      conn = post(conn, Routes.endpoint_log_path(conn, :create), endpoint_log: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.endpoint_log_path(conn, :show, id))

      assert %{
               "id" => ^id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.endpoint_log_path(conn, :create), endpoint_log: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update endpoint_log" do
    setup [:create_endpoint_log]

    test "renders endpoint_log when data is valid", %{conn: conn, endpoint_log: %EndpointLog{id: id} = endpoint_log} do
      conn = put(conn, Routes.endpoint_log_path(conn, :update, endpoint_log), endpoint_log: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.endpoint_log_path(conn, :show, id))

      assert %{
               "id" => ^id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, endpoint_log: endpoint_log} do
      conn = put(conn, Routes.endpoint_log_path(conn, :update, endpoint_log), endpoint_log: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete endpoint_log" do
    setup [:create_endpoint_log]

    test "deletes chosen endpoint_log", %{conn: conn, endpoint_log: endpoint_log} do
      conn = delete(conn, Routes.endpoint_log_path(conn, :delete, endpoint_log))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.endpoint_log_path(conn, :show, endpoint_log))
      end
    end
  end

  defp create_endpoint_log(_) do
    endpoint_log = endpoint_log_fixture()
    %{endpoint_log: endpoint_log}
  end
end
