defmodule MonitoringWeb.MonitorControllerTest do
  use MonitoringWeb.ConnCase

  import Monitoring.MonitorsFixtures

  alias Monitoring.Monitors.Monitor

  @create_attrs %{

  }
  @update_attrs %{

  }
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all monitors", %{conn: conn} do
      conn = get(conn, Routes.monitor_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create monitor" do
    test "renders monitor when data is valid", %{conn: conn} do
      conn = post(conn, Routes.monitor_path(conn, :create), monitor: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.monitor_path(conn, :show, id))

      assert %{
               "id" => ^id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.monitor_path(conn, :create), monitor: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update monitor" do
    setup [:create_monitor]

    test "renders monitor when data is valid", %{conn: conn, monitor: %Monitor{id: id} = monitor} do
      conn = put(conn, Routes.monitor_path(conn, :update, monitor), monitor: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.monitor_path(conn, :show, id))

      assert %{
               "id" => ^id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, monitor: monitor} do
      conn = put(conn, Routes.monitor_path(conn, :update, monitor), monitor: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete monitor" do
    setup [:create_monitor]

    test "deletes chosen monitor", %{conn: conn, monitor: monitor} do
      conn = delete(conn, Routes.monitor_path(conn, :delete, monitor))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.monitor_path(conn, :show, monitor))
      end
    end
  end

  defp create_monitor(_) do
    monitor = monitor_fixture()
    %{monitor: monitor}
  end
end
