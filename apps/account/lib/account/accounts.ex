defmodule Account.Accounts do
  @moduledoc """
  The Accounts context.
  """

  #TODO configurable
  @monitor_limit 20

  import Ecto.Changeset
  alias Account.Repo

  alias Account.Accounts.User

  def get_user!(id), do: Repo.get!(User, id)

  def init_user(attrs \\ %{}) do
    %User{}
    |> User.signup_changeset(attrs)
    |> Repo.insert()
  end

  def validate_username_password(attrs \\ %{}) do
    with {:ok, data} <- User.signin_changeset(%User{}, attrs) |> apply_action(:update) do
      case ArangoXEcto.aql_query(
        Repo,
        """
        FOR u in users FILTER u.username == "#{data.username}" LIMIT 1 RETURN u
        """
      ) do
        {:ok, [res]} ->
          user = ArangoXEcto.raw_to_struct(res, User)
          case Argon2.verify_pass(data.password, user.password) do
            true -> {:ok, user}
            false -> {:error, :unauthorized}
          end
        _ ->
          {:error, :not_found}
      end
    end
  end

  def check_limit(user_id) do
    case ArangoXEcto.aql_query(
        Repo,
        """
        FOR u in users FILTER u._key == "#{user_id}" LIMIT 1 RETURN u.monitors_count
        """
      ) do
        {:ok, [res]} ->
          cond do
            res <= @monitor_limit -> :ok
            true -> {:error, :endpoint_limit}
          end
        _ ->
          {:error, :not_found}
      end
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

end
