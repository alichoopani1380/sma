defmodule Account.Repo do
  use Ecto.Repo,
    otp_app: :account,
    adapter: ArangoXEcto.Adapter
end
