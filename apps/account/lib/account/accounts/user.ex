defmodule Account.Accounts.User do
  use ArangoXEcto.Schema
  import Ecto.Changeset

  schema "users" do
    field :username, :string, unique: :true
    field :password, :string
    field :phone_number, :string

    field :monitors_count, :integer, default: 0

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :password, :phone_number, :monitors_count])
    |> validate_required([:username])
  end

  def signup_changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :password, :phone_number])
    |> validate_required([:username, :password])
    |> put_password_hash()
  end

  def signin_changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :password])
    |> validate_required([:username, :password])
  end

  defp put_password_hash(changeset) do
    with %Ecto.Changeset{valid?: true, changes: %{password: password}} <- changeset do
          put_change(changeset, :password, Argon2.hash_pwd_salt(password))
    end
  end

end
