defmodule Account.Auth.Manager do
  use Guardian, otp_app: :account

  alias Account.Accounts

  def subject_for_token(user, _claims) do
    {:ok, to_string(user.id)}
  end

  def resource_from_claims(%{"sub" => id}) do
    # user = Accounts.get_user!(id)
    {:ok, %{id: id}}
  rescue
    Ecto.NoResultsError -> {:error, :resource_not_found}
  end
end
