defmodule Account.Auth.Pipeline do
  use Guardian.Plug.Pipeline, otp_app: :account,
  module: Account.Auth.Manager,
  error_handler: Account.Auth.ErrorHandler

  plug Guardian.Plug.VerifyHeader, realm: "Bearer"
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource
  plug :jwt_claims

  defp jwt_claims(conn, _opt) do
    Map.update(conn, :assigns, %{}, fn x -> Map.merge(x, %{user_id: conn.private.guardian_default_resource.id}) end)
  end
end
