defmodule Account.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Account.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: Account.PubSub}
      # Start a worker by calling: Account.Worker.start_link(arg)
      # {Account.Worker, arg}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Account.Supervisor)
  end
end
