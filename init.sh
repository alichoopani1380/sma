#!/bin/bash

if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi


jwt=$(curl -X POST --data "{\"username\": \"$DB_USERNAME\", \"password\": \"$DB_PASSWORD\"}" ${DB_ENDPOINT}/_open/auth | python3 -c "import sys, json; print(json.load(sys.stdin)['jwt'])")
echo "\n"
# db
curl -X POST -H "Authorization: Bearer $jwt" --data "{\"name\": \"$DB_NAME\"}" ${DB_ENDPOINT}/_api/database &&
echo "\n"
# collection: endpoint_logs
curl -X POST -H "Authorization: Bearer $jwt" --data '{"name": "endpoint_logs"}' ${DB_ENDPOINT}/_db/${DB_NAME}/_api/collection &&
echo "\n"
# collection: profiles
curl -X POST -H "Authorization: Bearer $jwt" --data '{"name": "monitors"}' ${DB_ENDPOINT}/_db/${DB_NAME}/_api/collection &&
echo "\n"
# collection: users
curl -X POST -H "Authorization: Bearer $jwt" --data '{"name": "users"}' ${DB_ENDPOINT}/_db/${DB_NAME}/_api/collection
echo "\n"
# collection: warnings
curl -X POST -H "Authorization: Bearer $jwt" --data '{"name": "warnings"}' ${DB_ENDPOINT}/_db/${DB_NAME}/_api/collection
echo "\n"

