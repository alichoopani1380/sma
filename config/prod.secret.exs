# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
use Mix.Config

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
#     config :nft_web, NftWeb.Endpoint, server: true
#
# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.

# Webs configs

db_name = System.get_env("DB_NAME") || raise "environment variable DB_NAME is missing"
db_username =  System.get_env("DB_USERNAME") || raise "environment variable DB_USERNAME is missing"
db_password =  System.get_env("DB_PASSWORD") || raise "environment variable DB_PASSWORD is missing"
db_endpoints = System.get_env("DB_ENDPOINT") || raise "environment variable DB_ENDPOINT is missing"
secret_key_base_account = System.get_env("SECRET_KEY_BASE_ACCOUNT") || raise "environment variable SECRET_KEY_BASE_ACCOUNT is missing."
secret_key_base_monitoring = System.get_env("SECRET_KEY_BASE_MONITORING") || raise "environment variable SECRET_KEY_BASE_MONITORING is missing."

config :account, Account.Repo,
  username: db_username,
  password: db_password,
  database: db_name,
  endpoints: db_endpoints,
  # show_sensitive_data_on_connection_error: true

config :nft, Monitoring.Repo,
  username: db_username,
  password: db_password,
  database: db_name,
  endpoints: db_endpoints,
  # show_sensitive_data_on_connection_error: true

config :accounts_web, AccountsWeb.Endpoint,
http: [
  port: 4000,
  transport_options: [socket_opts: [:inet6]]
],
secret_key_base: account_secret_key_base

config :nft_web, NftWeb.Endpoint,
  http: [
    port: 4001,
    transport_options: [socket_opts: [:inet6]]
  ],
  secret_key_base: secret_key_base_monitoring
