import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :monitoring_web, MonitoringWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "X6NkWj1ctDZZ8tSUOmBc/zzjuyaicKnv1EBuVsQgsqLuU/30vU8e1ESX2u7QclV3",
  server: false

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :monitoring_web, MonitoringWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "+jcTFR3ShzULtqpjM2m4ccR9fiKAJEzEu4BHX57t1soSvvd1l6mcFFVSKSevUAvE",
  server: false

config :monitoring, Monitoring.Repo,
  database: System.get_env("DB_NAME", "smapp"),
  endpoints: System.get_env("DB_URL", "http://localhost:8529"),
  username: System.get_env("DB_USERNAME", "root"),
  password: System.get_env("DB_PASSWORD", "test_password"),
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

# Configure your database
config :account, Account.Repo,
  database: System.get_env("DB_NAME", "smapp"),
  endpoints: System.get_env("DB_URL", "http://localhost:8529"),
  username: System.get_env("DB_USERNAME", "root"),
  password: System.get_env("DB_PASSWORD", "test_password"),
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :account_web, AccountWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "GKMoCMFxlxvMwcy2ioZWx51E9oCBTmMB2V+gYvrB9bJZj/wjYTyjCpzz9aKmTzVw",
  server: false
